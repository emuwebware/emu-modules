<?php

class emuModules extends emuApp
{
    public function setAppConfig()
    {
        $this->emuAppID = 'emuMO';
        $this->menuName = 'Emu Modules';
        $this->dbPrefix = '_emu_modules';
    }

    public function init()
    {
        include_once $this->pluginPath.'/include/functions.php';

        $this->addShortCode('emu_module', array($this, 'doModuleShortCode'));
    }


    function doModuleShortCode( $atts, $content = '', $tag )
    {
        $module_name = $atts['name'];

        // exit($module_name);

        $module = $this->getModule($module_name);

        include( $module->path );
    }

    function getCoreModules()
    {
        $modules = $this->getFiles( $this->pluginPath.'/modules' );
        $emu_modules = array();

        if ( is_array( $modules ) ) {

            $base = array( trailingslashit( $this->pluginPath ) );

            foreach ( $modules as $module )
            {
                if( file_exists( $module ) )
                {
                    $basename = str_replace($base, '', $module);

                    $module_data = file( $module );
                    $module_data = implode( '', $module_data );

                    $name = $this->getModuleName( $module_data );

                    if ( !empty( $name ) ) {
                        $name = trim( $name );
                        $emu_modules[$name] = (object) array( 'name' => $name, 'path' => $module, 'filename' => basename( $module ), 'uri' => $this->pluginURLPath.'/modules/'.basename( $module ), 'directory' => dirname( $module ), 'content' => '', 'isWritable' => $this->isWritable($module), 'isCore' => true );
                    }
                }
            }
        }
        return $emu_modules;
    }

    function getModuleName ( $module_content )
    {
        $name = '';
        if ( preg_match( '|Emu Module:(.*)$|mi', $module_content, $name ) )
            $name = _cleanup_header_comment($name[1]);

        return $name;
    }

    function getModule( $module_name )
    {
        $modules = $this->getModules();

        if(!in_array( $module_name, array_keys($modules) ) ) return '';

        $module = $modules[ $module_name ];
        $module->content = file_get_contents( $module->path );

        return $module;
    }

    function getThemeModules()
    {
        global $wp_version;

        if( version_compare( $wp_version, '3.4', '<' ) )
        {
            $themes = get_themes();
            $theme = get_current_theme();
            $templates = $themes[$theme]['Template Files'];
        }
        else
        {
            $theme = wp_get_theme();
            $templates = $theme['Template Files'];
        }

        $emu_modules = array();

        if ( is_array( $templates ) ) {
            $base = array( trailingslashit(get_template_directory()), trailingslashit(get_stylesheet_directory()) );

            foreach ( $templates as $template ) {

                if( file_exists( $template ) )
                {
                    $basename = str_replace($base, '', $template);

                    // don't allow template files in subdirectories
                    $template_data = file( $template );
                    $template_data = implode( '', $template_data );

                    $name = $this->getModuleName( $template_data );

                    if ( !empty( $name ) ) {
                        $name = trim( $name );
                        $emu_modules[$name] = (object) array( 'name' => $name, 'path' => $template, 'filename' => $basename, 'uri' => get_bloginfo('stylesheet_directory').'/'.$basename, 'directory' => get_stylesheet_directory(), 'content' => '', 'isWritable' => $this->isWritable($template), 'isCore' => false );
                    }
                }
            }
        }

        return $emu_modules;

    }

    function getModules()
    {
        return array_merge( $this->getCoreModules(), $this->getThemeModules() );
    }


    function commentListFormat( $comment, $args, $depth )
    {
        $GLOBALS['comment'] = $comment;

        switch ( $comment->comment_type )
        {
            case '':
            ?>
                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

                    <div id="comment-<?php comment_ID(); ?>" class="emu-comment">

                        <div class="emu-comment-avatar">
                            <?php echo get_avatar( $comment, 40 ); ?>
                        </div>

                        <div class="emu-comment-author">
                            <?php echo get_comment_author_link(); ?>
                        </div>

                        <div class="emu-comment-details">
                            <a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>"><?php comment_date() ?> at <?php comment_time() ?></a>
                        </div>

                        <div class="comment-body"><?php comment_text(); ?></div>

                        <?php
                        if ( $comment->comment_approved == '0' )
                        {
                            ?><p class="emu-comment-moderation">This comment is awaiting moderation</p><?php
                        }
                        ?>

                        <div class="emu-reply-link">
                            <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                        </div>

                    </div>

            <?php
            break;
            case 'pingback': // fall-through
            case 'trackback':
            ?>
                <li class="emu-pingback post pingback">
                    <p>Pingback: <?php comment_author_link(); ?><?php edit_comment_link( '(Edit)', ' ' ); ?></p>

            <?php
            break;
        }
        // Note: the closing </li> tag is added by wordpress
    }

}

?>