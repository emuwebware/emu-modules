<?php
/*
Plugin Name: Emu Modules
Plugin URI: http://www.emuwebware.com
Description: Load modules via short codes and widgets
Version: 0.1
Author: Emu
Author URI: http://www.emuwebware.com
*/

$plugin_init =  'include_once( "class/main.class.php" ); '.
                'global $emuModules; '.
                '$emuModules = new emuModules("'.__FILE__.'");';

add_action( 'emu_framework_loaded', create_function('', $plugin_init) );


?>
