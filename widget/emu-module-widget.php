<?php

/*
Emu Widget: Emu Module
Emu Widget Class: emuModuleWidget
Emu Widget Description: Include modules using widgets
*/

/**
 * emuModuleWidget Class
 */
class emuModuleWidget extends WP_Widget
{
	public $defaults = array('title' => '', 'module_name' => '');

	/** constructor */
	function emuModuleWidget()
	{
		parent::WP_Widget( false, $name = 'Emu Module' );
	}

    function widget($widget_args, $instance) {

		global $wpdb;
		global $emuModules;

		extract( $widget_args );

		$instance_args = wp_parse_args( $instance, $this->defaults );

		extract( $instance_args );

		$title = apply_filters( 'widget_title', $title );

		echo $before_widget;

		if ( $title ) echo $before_title . $title . $after_title;

		include_emu_module( $module_name );

		echo $after_widget;
	}

    function update($new_instance, $old_instance) { return $new_instance; }

	function form($instance)
	{
		$args = wp_parse_args( $instance, $this->defaults );
		extract( $args );

		global $emuModules;

		$arr_modules = array();

		if( $modules = $emuModules->getModules() )
		{
			foreach( $modules as $module )
				$arr_modules[] = $module->name;
		}

		?>
		<div class="emu-widget-control">
			<div>
				<label for="<?php echo $this->get_field_id('title'); ?>">
				Title (optional):</label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</div>
			<div>
				<label><strong>Module</strong>:</label>
				<?php echo drop_down( $this->get_field_id('module_name'),$this->get_field_name('module_name'), 'widefat', $module_name, $arr_modules, 'None' ) ?>
			</div>
		</div>

		<?php
    }
}



?>