<?php

/*
Emu Module: Page/Post Content (with comments)
*/ 

?>

<?php while ( have_posts() ) : the_post(); ?>

<?php the_title('<h1>', '</h1>'); ?>

<?php the_content(); ?>

<?php comments_template();?>

<?php endwhile; ?>
