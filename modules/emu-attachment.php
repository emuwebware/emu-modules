<?php

/*
Emu Module: Emu Attachment
*/ 

if ( have_posts() ) while ( have_posts() ) : the_post(); 
?>

<?php
if( !empty( $post->post_parent ) )
{
	?>
	<div class="emu-navigation">
		<div class="emu-nav-previous">&laquo; Return to <a href="<?php echo get_permalink( $post->post_parent ); ?>" title="<?php esc_attr( 'Return to '.get_the_title( $post->post_parent ) ); ?>" rel="gallery">
		<?php echo get_the_title( $post->post_parent )?>
		</a></div>
	</div>
	<?php
}
?>

<div class="emu-attachment">
	
	<h1 class="emu-attachment-title"><?php the_title(); ?></h1>
	
	<?php
	/*
	<p class="emu-attachment-meta">
		By echo sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>', get_author_posts_url( get_the_author_meta( 'ID' ) ), sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ), get_the_author() );
	</p>
	*/
	?>
	
	<?php
	if ( wp_attachment_is_image() ) 
	{
		$metadata = wp_get_attachment_metadata();
		
		?>
		<p class="emu-image-meta">
		
		Full size is <?php echo sprintf( '<a href="%1$s" title="%2$s">%3$s &times; %4$s</a>', wp_get_attachment_url(), 'Link to full-size image', $metadata['width'], $metadata['height'] ); ?>
		
		</p>
		<?php
	}
	?>

	<?php edit_post_link( 'Edit', '<p class="emu-edit-link">', '</p>' ); ?>

	<?php
	
	if ( wp_attachment_is_image() )
	{
		$attachments = array_values( get_children( array( 'post_parent' => $post->post_parent, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID' ) ) );
		foreach ( $attachments as $k => $attachment ) 
		{
			if ( $attachment->ID == $post->ID ) break;
		}
		
		$k++;
		
		// If there is more than 1 image attachment in a gallery
		if ( count( $attachments ) > 1 ) 
		{
			if ( isset( $attachments[ $k ] ) )
				// get the URL of the next image attachment
				$next_attachment_url = get_attachment_link( $attachments[ $k ]->ID );
			else
				// or get the URL of the first image attachment
				$next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID );
		} 
		else 
		{
			// or, if there's only 1 image attachment, get the URL of the image
			$next_attachment_url = wp_get_attachment_url();
		}
		?>
		<p class="attachment">
			<a href="<?php echo $next_attachment_url; ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment"><?php echo wp_get_attachment_image( $post->ID, 'emu-attachment' ); ?></a>
		</p>

		<div class="entry-caption"><?php if ( !empty( $post->post_excerpt ) ) the_excerpt(); ?></div>

		<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>' ); ?>
	
		<div class="emu-navigation">
			<div class="emu-nav-next"><?php next_image_link( '', 'Next image &raquo;' ); ?> </div>
			<div class="emu-nav-previous"><?php previous_image_link( '', '&laquo; Previous image' ); ?></div>
		</div>		

		<?php
	}
	else
	{
		?>
		<a href="<?php echo wp_get_attachment_url(); ?>" title="<?php echo esc_attr( get_the_title() ); ?>" rel="attachment"><?php echo basename( get_permalink() ); ?></a>
		<?php
	}
	?>
	
	
	<?php wp_link_pages( array( 'before' => '<div class="page-link">Pages:', 'after' => '</div>' ) ); ?>
	
	<?php 
	// comments_template(); 
	?>

</div>
<!-- / emu-attachment -->
	
<?php 
endwhile; 
?>



