<?php

/*
Emu Module: Page/Post Content (with comments, no title)
*/ 

?>

<div class="white_bg">
  
<?php while ( have_posts() ) : the_post(); ?>

<?php the_content(); ?>

<?php endwhile; ?>

</div>