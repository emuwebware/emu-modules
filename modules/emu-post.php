<?php

/*
Emu Module: Emu Post
*/

?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="emu-navigation">
  <div class="emu-nav-next"><?php next_post_link('%link &raquo;', 'Next', true); ?></div>
  <div class="emu-nav-previous"><?php previous_post_link('&laquo; %link', 'Previous', true); ?></div>
</div>

<div id="emu-post-single" class="emu-post">

  <!-- Post category -->
  <p class="emu-post-category">
    Posted in <?php the_category(', '); ?>
  </p>

  <h1 class="emu-post-title"><?php the_title(); ?></h1>

  <!-- Post author -->
  <p class="emu-post-author">
    By <?php the_author_link(); ?>
  </p>

  <!-- Post date -->
  <div class="emu-post-date">
    <span class="emu-day"><?php echo get_the_date('d'); ?></span>
    <div class="emu-month-year">
      <span class="emu-month"><?php echo get_the_date('M'); ?></span>
      <span class="emu-year"><?php echo get_the_date('Y'); ?></span>
    </div>
    <span class="emu-time"><?php echo get_the_date('h:sa'); ?></span>
  </div>

  <?php
  /*
  <!-- Post details -->
  <p class="emu-post-details">By <a href=""><?php echo get_the_author()?></a> on <em><?php echo get_the_date();?></em>, posted in <?php the_category(', ') ?><br /><?php the_tags();?></p>
  */
  ?>

  <!-- Post tags -->
  <?php the_tags( '<p class="emu-post-tags">Tags <span class="emu-tag">', '</span><span class="emu-tag">', '</span></p>' ); ?>

  <!-- Post content -->
  <p class="emu-post-content">
    <?php the_content(); ?>
  </p>

  <?php edit_post_link( 'Edit Entry', '<p class="emu-edit-link">', '</p>' ); ?>

  <?php comments_template(); ?>

  <?php
  /*
  previous_post_link('<div class="emu-previous-post">Previous post: %link</div>');
  next_post_link('<div class="emu-next-post">Next post: %link</div>');
  */
  ?>

</div>

<div class="emu-navigation">
  <div class="emu-nav-next"><?php next_post_link('%link &raquo;', 'Next', true); ?></div>
  <div class="emu-nav-previous"><?php previous_post_link('&laquo; %link', 'Previous', true); ?></div>
</div>

<?php endwhile; ?>



