<?php

/*
Emu Module: Emu Page
*/ 

?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="emu-page">

	<!-- Post title -->
	<h1 class="emu-page-title"><?php the_title(); ?></h1>
	
	<!-- Post details -->
	<p class="emu-post-details">By <a href=""><?php echo get_the_author()?></a> on <em><?php echo get_the_date();?></em></p>
		
	<!-- Post excerpt -->
	<p class="emu-post-content">
		<?php the_content(); ?>
	</p>
	
	<?php comments_template(); ?>
	
	<?php edit_post_link( 'Edit Entry', '<p class="emu-edit-link">', '</p>' ); ?>
	
</div>

<?php endwhile; ?>



