<?php

/*
Emu Module: Emu Blog
*/ 

?>

<!-- Pagination -->
<?php emu_pagination( '<div class="emu-pagination">', '</div>'); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="emu-post emu-clear-left">
  
  <!-- Post category -->
  <p class="emu-post-category">
    Posted in <?php the_category(', '); ?>
  </p>

  <!-- Post title -->
  <h2 class="emu-post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
  
  <!-- Post date - separate components -->
  <div class="emu-post-date">
    <span class="emu-day"><?php echo get_the_date('d'); ?></span>
    <div class="emu-month-year">
      <span class="emu-month"><?php echo get_the_date('M'); ?></span>
      <span class="emu-year"><?php echo get_the_date('Y'); ?></span>
    </div>
  </div>
  
  <?php
  /*
  <!-- Post details -->
  <p class="emu-post-details">By <a href=""><?php echo get_the_author()?></a> on <em><?php echo get_the_date();?></em>, posted in <?php the_category(', ') ?><br /><?php the_tags();?></p>
  */
  ?>
  
  <!-- Post author -->
  <p class="emu-post-author">
    By <?php the_author_link(); ?>
  </p>
  
  <!-- Post excerpt -->
  <p class="emu-post-excerpt">
    
    <!-- Post thumbnail -->
    <?php if ( has_post_thumbnail() ) : ?>
    <div class="emu-post-thumbnail"><?php the_post_thumbnail('thumbnail');?></div>
    <?php endif; ?>
    
    <?php the_excerpt(); ?>
  </p>
  
  <!-- Post tags -->
  <?php the_tags( '<p class="emu-post-tags">Tags <span class="emu-tag">', '</span><span class="emu-tag">', '</span></p>' ); ?> 

  <p class="emu-post-comments"><?php comments_number( 'No Comments', '1 Comment', '% responses' ); ?></p>
  
  <?php edit_post_link( 'Edit Entry', '<p class="emu-edit-link">', '</p>' ); ?>
  
</div>

<?php endwhile; ?>

<!-- Pagination -->
<?php emu_pagination( '<div class="emu-pagination">', '</div>'); ?>


