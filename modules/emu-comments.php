<?php
/*
Emu Module: Emu Comments
*/

global $emuModule;

?>

<div id="emu-comments">

	<?php

	if( post_password_required() )
	{
		echo '<p class="emu-password-required">This post is password protected. Enter the password to view any comments</p>';
		echo '</div>';
		return;
	}

	if( have_comments() )
	{
		?>

		<!-- Comments title -->
		<h3 class="emu-comments-title"><?php comments_number( 'No Comments', '1 Comment', '% responses' ); ?></h3>

		<!-- Comment navigation -->
		<?php
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) )
		{
			?>
			<p class="emu-comments-previous">&laquo; <?php previous_comments_link( 'Older Comments' ); ?></p>
			<p class="emu-comments-next"><?php next_comments_link( 'Newer Comments' ); ?> &raquo;</p>
			<?php
		}
		?>

		<!-- Comments -->
		<ol class="emu-comments-list">
			<?php wp_list_comments( array( 'callback' => array( $emuModule, 'commentListFormat' ) ) ); ?>
		</ol>

		<!-- Comment navigation -->
		<?php
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) )
		{
			?>
			<p class="emu-comments-previous">&laquo; <?php previous_comments_link( 'Older Comments' ); ?></p>
			<p class="emu-comments-next"><?php next_comments_link( 'Newer Comments' ); ?> &raquo;</p>
			<?php
		}
		?>

		<?php
	}
	?>

	<?php comment_form(); ?>

</div>

