<?php

/*
Emu Module: Emu Category
*/ 

?>

<h1 class="emu-category-title">Category: <?php echo single_cat_title( '', false ) ?></h1>

<?php
	$category_description = category_description();
	if ( !empty( $category_description ) ) echo '<div class="emu-category-desc">' . $category_description . '</div>';
?>

<!-- Pagination -->
<?php emu_pagination( '<div class="emu-pagination">', '</div>'); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="emu-post">

	<!-- Post category -->
	<p class="emu-post-category">
		Posted in <?php the_category(', '); ?>
	</p>

	<!-- Post title -->
	<h2 class="emu-post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	
	<!-- Post date - separate components -->
	<div class="emu-post-date">
		<span class="emu-day"><?php echo get_the_date('d'); ?></span>
		<div class="emu-month-year">
			<span class="emu-month"><?php echo get_the_date('M'); ?></span>
			<span class="emu-year"><?php echo get_the_date('Y'); ?></span>
		</div>
		<span class="emu-time"><?php echo get_the_date('h:sa'); ?></span>
	</div>

	<!-- Post author -->
	<p class="emu-post-author">
		By <?php the_author_link(); ?>
	</p>
	
	<!-- Post excerpt -->
	<p class="emu-post-excerpt">
		
		<!-- Post thumbnail -->
		<?php if ( has_post_thumbnail() ) : ?>
		<div class="emu-post-thumbnail"><?php the_post_thumbnail();?></div>
		<?php endif; ?>
		
		<?php the_excerpt(); ?>
	</p>
	
	<!-- Post tags -->
	<?php the_tags( '<p class="emu-post-tags">Tags <span class="emu-tag">', '</span><span class="emu-tag">', '</span></p>' ); ?> 
	
	<!-- Post comments -->
	<p class="emu-post-comments">
		<?php comments_number( 'No Comments', '1 Comment', '% responses' ); ?>
	</p>
	
	<?php edit_post_link( 'Edit Entry', '<p class="emu-edit-link">', '</p>' ); ?>
	
</div>

<?php endwhile; ?>

<!-- Pagination -->
<?php emu_pagination( '<div class="emu-pagination">', '</div>'); ?>


