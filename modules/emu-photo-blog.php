<?php

/*
Emu Module: Emu Photo Blog
*/ 

?>

<!-- Pagination -->
<?php emu_pagination( '<div class="emu-pagination">', '</div>'); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="emu-post">
	
	<!-- Post category -->
	<p class="emu-post-category">
		Posted in <?php the_category(', '); ?>
	</p>

	<!-- Post title -->
	<h2 class="emu-post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	
	<!-- Post date - separate components -->
	<div class="emu-post-date">
		<span class="emu-day"><?php echo get_the_date('d'); ?></span>
		<div class="emu-month-year">
			<span class="emu-month"><?php echo get_the_date('M'); ?></span>
			<span class="emu-year"><?php echo get_the_date('Y'); ?></span>
		</div>
		<span class="emu-time"><?php echo get_the_date('h:sa'); ?></span>
	</div>
	
	<!-- Post author -->
	<p class="emu-post-author">
		By <?php the_author_link(); ?>
	</p>
	
	<!-- Post excerpt -->
	<p class="emu-post-images">
		<?php
		$attachments = get_children( array( 'post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID' ) );
		
		if( count( $attachments ) > 0 )
		{
			foreach ( $attachments as $attachment ) 
			{
				$image_attributes = wp_get_attachment_image_src( $attachment->ID, 'medium' )
				?>
				<a href="<?php echo get_permalink($attachment->ID); ?>"><img src="<?php echo $image_attributes[0] ?>" width="<?php echo $image_attributes[1] ?>" height="<?php echo $image_attributes[2] ?>"></a>
				<?php
			}
		}
		else
		{
			echo '<em>No images for this post</em>';
		}
		?>
	</p>
	
	<!-- Post tags -->
	<?php the_tags( '<p class="emu-post-tags">Tags <span class="emu-tag">', '</span><span class="emu-tag">', '</span></p>' ); ?> 
	
	<!-- Post comments -->
	<p class="emu-post-comments">
		<?php comments_number( 'No Comments', '1 Comment', '% responses' ); ?>
	</p>
	
	<?php edit_post_link( 'Edit Entry', '<p class="emu-edit-link">', '</p>' ); ?>
	
</div>

<?php endwhile; ?>

<!-- Pagination -->
<?php emu_pagination( '<div class="emu-pagination">', '</div>'); ?>


