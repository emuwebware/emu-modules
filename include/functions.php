<?php

function include_emu_module($module_name)
{
    global $emuModules;

    if( !$module = $emuModules->getModule($module_name) ) return false;
    include_once( $module->path );
}

function get_emu_module($module_name)
{
    ob_start();

    include_emu_module($module_name);

    $module_content = ob_get_contents();

    ob_end_clean();

    return $module_content;
}

?>